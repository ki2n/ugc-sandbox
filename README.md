# ugc-sandbox

A launching script for [Ungoogled] Chromium to add sandboxing via Firejail.

## Usage
`chromium-sandbox [low | mid | high | highest] [URL]`
* `low`: Basic sandbox; use actual `$HOME/.config/chromium` profiles
* `mid`: Basic sandbox + using `localhost` as DNS (DNSCrypt must be running or no pages will load) + `UserAgentClientHint` disabled
* `high`: Chromium (temporary) profile (with extensions preloaded)\* loaded onto a `ramfs` filesystem + `localhost` as DNS + `UserAgentClientHint` disabled
* `highest`: `firejail --private` (no extensions) + `localhost` DNS + `UserAgentClientHint` disabled

## Configuration
The settings for `chromium-sandbox` exists currently at `/etc/ugcsrc`, but in future revisions may see an option to have per-user configuration options.

## Installation
### Dependencies
* [Ungoogled Chromium](https://github.com/Eloston/ungoogled-chromium) (preferred), or [Chromium](https://www.chromium.org/Home/)\*\*
* Firejail
* Polkit
* DNSCrypt
* `ssh-askpass`\*\*\* (optional alternative to Polkit, requires X)

\* You will have to build your own custom profile to fit your needs, using `chromium --user-data-dir=${PROFILE_PATH}`, and compress it into an archive as `ugcs.tar.xz`, in `/opt/ugc-sandbox`.

\*\* There is a repsitory for automated builds for `ungoogled-chromium` for Debian/Ubuntu but they only specifically support a couple of release versions so therefore in the Debian/Ubuntu packages the [Ungoogled] Chromium dependency is left out if you choose to use the contributor binaries for `ungoogled-chromium`, or build the binaries yourself.

\*\*\* This package will be named differently in different distros:
* `ssh-askpass` in Debian/Ubuntu
* `x11-ssh-askpass` in Arch


### Arch Linux
Download the `PKGBUILD` from [Releases](https://gitlab.com/ki2n/ugc-sandbox/-/releases), and run `makepkg -si`.
### Debian/Ubuntu
Available in [Releases](https://gitlab.com/ki2n/ugc-sandbox/-/releases).

**NOTE:** Currently the released .deb file only targets Ubuntu 20.04LTS+ (A Debian version is in the works) and does include the `ungoogled-chromium` dependency. Use the guide [here](https://github.com/ungoogled-software/ungoogled-chromium-debian) to add the OBS repository.
### Fedora (TBA)
Prebuilt .rpm packages TBA.
### Manual installation (distribution-agnostic)
Clone this repository using `git clone https://gitlab.com/ki2n/ugc-sandbox.git`, then copy or symlink `chromium-sandboxed` to your bin directory (`/bin`, `/usr/bin`), `chromium-sandboxed.1` to the default path where your manual pages are located, and finally the configuration file `ugcsrc` to `/etc`.
## Contributing
As this is mostly a personal private project intended for personal use and with personal use cases in mind, no pull requests will be accepted. If you wish to improve anything in part or its entirety, please do make a fork of this repository and modify it to your needs.
## License
BSD-3-clause. See [LICENSE](https://gitlab.com/ki2n/ugc-sandbox/-/blob/main/LICENSE)
